var cart = {};
var products = {};
var retryLimit = 5;
var retryCount = 0;
var inactiveTime = 0;
var inactiveTimer = setInterval(onTimerTick, 1000);

function getProducts() {
  if (retryCount == retryLimit) {
    console.log("Tried to connect 5 times and failed...");
    alert("Tried to connect 5 times and failed...");
    retryCount = 0;
    return;
  }
  retryCount++;
  var xhttp = new XMLHttpRequest();
  xhttp.timeout = 2000;
  xhttp.onload = function() {
    if (xhttp.status == 200) {
      products = JSON.parse(xhttp.responseText);
      retryCount = 0;
      initializeProducts();
      showProductAddButton();
    } else {
      getProducts();
    }
  }
  xhttp.onerror = function() {
    console.log("Tried" + retryCount + "times to connect and failed, retrying...");
    getProducts();
  }
  xhttp.ontimeout = function() {
    console.log("Tried" + retryCount + "times to connect and timed out, retrying...");
    getProducts();
  }
  xhttp.open("GET", "https://cpen400a.herokuapp.com/products", true);
  xhttp.send();
}

function getCheckoutProducts() {
  if (retryCount == retryLimit) {
    console.log("Tried 5 times to connect and failed...");
    alert("Tried to connect 5 times and failed...");
    retryCount = 0;
    return;
  }
  retryCount++;
  var xhttp = new XMLHttpRequest();
  xhttp.onload = function() {
    if (xhttp.status == 200) {
      products = JSON.parse(xhttp.responseText);
      retryCount = 0;
      showProductAddButton();
      var missingprod = "";
      for (var productName in cart) {
        var quantity = cart[productName];
        if (quantity > products[productName]["quantity"]) {
          while (quantity > products[productName]["quantity"]) {
            removeFromCart(productName);
            quantity--;
          }
          missingprod += productName + "\n";
        }
      }
      if (missingprod.length > 0) {
        alert("Product quantities mismatch on server:\n" + missingprod + "\nThe quantities have been updated, click checkout again to proceed.");
      } else {
        alert("Purchase successful, total price: $" + document.getElementById("cartPrice").innerHTML);
        location.reload();
      }
    } else {
      getCheckoutProducts();
    }
  }
  xhttp.onerror = function() {
    console.log("Tried" + retryCount + "times to connect and failed, retrying...");
    getCheckoutProducts();
  }
  xhttp.ontimeout = function() {
    console.log("Tried" + retryCount + "times to connect and timed out, retrying...");
    getCheckoutProducts();
  }
  xhttp.open("GET", "https://cpen400a.herokuapp.com/products", true);
  xhttp.send();
}

function checkoutClick() {
  getCheckoutProducts();
  alert("Checking server to verify product quantities");
}

function updateCartPrice() {
  price = 0;
  for (var productName in cart) {
    var quantity = cart[productName];
    price += products[productName]["price"] * quantity;
  }
  cartPrice = document.getElementById("cartPrice");
  cartPrice.textContent = price;
  modalPrice = document.getElementById("modalPrice");
  modalPrice.textContent = price;
}

function showRemoveButton(productName) {
  document.getElementById(productName + "Remove").style.display = "inline-block";
}

function hideRemoveButton(productName) {
  document.getElementById(productName + "Remove").style.display = "none";
}

function addToCart(productName) {
  if (products[productName]["quantity"] == 0 || products[productName]["quantity"] == cart[productName]) {
    alert("The website does not have anymore " + productName + ", fool!");
  } else {
    if (productName in cart) {
      cart[productName]++;
    } else {
      cart[productName] = 1;
      showRemoveButton(productName);
    }
    resetTimer();
    updateCartPrice();
    updateModalRow(productName);
  }
}

function removeFromCart(productName) {
  if (!(productName in cart)) {
    alert("Product does not exist in the cart, fool!");
  } else {
    if (cart[productName] == 1) {
      delete cart[productName];
      hideRemoveButton(productName);
    } else {
      cart[productName]--;
    }
    resetTimer();
    updateCartPrice();
    updateModalRow(productName);
  }
}

function resetTimer() {
  clearInterval(inactiveTimer);
  inactiveTime = 0;
  document.getElementById("inactiveTime").textContent = inactiveTime;
  inactiveTimer = setInterval(onTimerTick, 1000);
}

function onTimerTick() {
  inactiveTime++;
  document.getElementById("inactiveTime").textContent = inactiveTime;
  if (inactiveTime % 300 == 0) {
    alert("Hey there! Are you still planning to buy something?");
  }
}

function showItem(productName) {
  return function() {
    alert(productName + ": " + cart[productName]);
  };
}

function showCart() {
  var i = 0;
  for (var productName in cart) {
    setTimeout(showItem(productName), i * 30000);
    i++;
  }
  return false;
}

function showOverlay() {
  document.getElementById("overlay").style.visibility = "visible";
}

function updateModalRow(productName) {
  var tr = document.getElementById(productName + "ModalRow");
  if (productName in cart) {
    var quantity = cart[productName];
    var price = products[productName]["price"] * quantity;
    tr.style.display = "table-row";
    tr.childNodes[1].innerHTML = quantity;
    tr.childNodes[2].innerHTML = price;
  } else {
    tr.style.display = "none";
  }
}

function closeModal() {
  document.getElementById("overlay").style.visibility = "hidden";
}

function hideCartModal(e) {
  if (e && e.keyCode == 27) {
    closeModal();
  }
}

function onClickAdd(productName) {
  return function() {
    addToCart(productName);
  };
}

function onClickRemove(productName) {
  return function() {
    removeFromCart(productName)
  }
}

function showProductAddButton() {
  for (var productName in products) {
    if (products[productName]["quantity"] > 0) {
      document.getElementById(productName + "Add").style.display = "inline";
    }
  }
}

function initializeProducts() {
  var tbl = document.getElementById("modalTable");
  var productCart = "<div class='product-cart'><img src='images/cart.png' /></div>";
  for (var productName in products) {
    var img = "<img src='" + products[productName]["url"] + "' />";
    var buttonAdd = "<button type='button' class='add' id='" + productName + "Add'>Add</button>";
    var buttonRemove = "<button type='button' class='remove' id='" + productName + "Remove'>Remove</button>";
    var productAddRemove = "<div class='product-add-remove'>" + buttonAdd + buttonRemove + "</div>";
    var productPrice = "<div class='product-price'>$" + products[productName]["price"] + "</div>";
    var productInner = "<div class='product-inner'>" + productCart + img + productAddRemove + productPrice + "</div>";
    var h3 = "<h3>" + productName + "</h3>";
    var productDiv = document.createElement("div");
    productDiv.className = "product";
    productDiv.innerHTML = productInner + h3;
    document.getElementById("productList").appendChild(productDiv);

    var idAdd = productName + "Add";
    document.getElementById(idAdd).addEventListener("click", onClickAdd(productName), false);
    document.getElementById(idAdd).style.display = "none";
    var idRemove = productName + "Remove";
    var removeButton = document.getElementById(idRemove);
    removeButton.addEventListener("click", onClickRemove(productName), false);
    removeButton.style.display = "none";

    var quantity = cart[productName];
    if (!quantity) {
      quantity = 0;
    }
    var price = products[productName]["price"] * quantity;
    var tr = tbl.insertRow();
    tr.id = productName + "ModalRow";
    tr.innerHTML = "<td>" + productName + "</td><td>" + quantity + "</td><td>" + price + "</td><td><span class='add'>+</span>/<span class='remove'>-</span></td>";
    tr.childNodes[3].childNodes[2].addEventListener("click", onClickRemove(productName), false);
    tr.childNodes[3].childNodes[0].addEventListener("click", onClickAdd(productName), false);
    tr.style.display = "none";
  }
}

getProducts();

document.getElementById("showCart").addEventListener("click", showCart, false);
document.getElementById("cartPriceContainer").addEventListener("click", showOverlay, false);
document.getElementById("closeModal").addEventListener("click", closeModal, false);
document.getElementById("checkout").addEventListener("click", checkoutClick, false);
document.addEventListener("keydown", hideCartModal, false);

