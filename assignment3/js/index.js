var cart = {
  "Box1": 0,
  "Box2": 0,
  "Clothes1": 0,
  "Clothes2": 0,
  "Jeans": 0,
  "Keyboard": 0,
  "KeyboardCombo": 0,
  "Mice": 0,
  "PC1": 0,
  "PC2": 0,
  "PC3": 0,
  "Tent": 0
};

var products = {
  "Box1": {
    "price": 10,
    "quantity": 9
  },
  "Box2": {
    "price": 5,
    "quantity": 9
  },
  "Clothes1": {
    "price": 20,
    "quantity": 9
  },
  "Clothes2": {
    "price": 30,
    "quantity": 9
  },
  "Jeans": {
    "price": 50,
    "quantity": 9
  },
  "Keyboard": {
    "price": 20,
    "quantity": 9
  },
  "KeyboardCombo": {
    "price": 40,
    "quantity": 9
  },
  "Mice": {
    "price": 20,
    "quantity": 9
  },
  "PC1": {
    "price": 350,
    "quantity": 9
  },
  "PC2": {
    "price": 400,
    "quantity": 9
  },
  "PC3": {
    "price": 300,
    "quantity": 9
  },
  "Tent": {
    "price": 100,
    "quantity": 9
  },
};

function updateCartPrice() {
  price = 0;
  for (var productName in cart) {
    var quantity = cart[productName];
    price += products[productName]["price"] * quantity;
  }
  cartPrice = document.getElementById("cartPrice");
  cartPrice.textContent = price;
  modalPrice = document.getElementById("modalPrice");
  modalPrice.textContent = price;
}

function showRemoveButton(productName) {
  document.getElementById(productName + "Remove").style.display = "inline-block";
}

function hideRemoveButton(productName) {
  document.getElementById(productName + "Remove").style.display = "none";
}

function addToCart(productName) {
  if (products[productName]["quantity"] == 0) {
    alert("The website does not have anymore " + productName + ", fool!");
  } else {
    cart[productName]++;
    products[productName]["quantity"]--;
    resetTimer();
    updateCartPrice();
    showRemoveButton(productName);
    updateModalRow(productName);
  }
}

function removeFromCart(productName) {
  if (cart[productName] == 0) {
    alert("Product does not exist in the cart, fool!");
  } else {
    cart[productName]--;
    products[productName]["quantity"]++;
    resetTimer();
    updateCartPrice();
    updateModalRow(productName);
    if (cart[productName] == 0) {
      hideRemoveButton(productName);
    }
  }
}

function resetTimer() {
  clearInterval(inactiveTimer);
  inactiveTime = 0;
  document.getElementById("inactiveTime").textContent = inactiveTime;
  inactiveTimer = setInterval(onTimerTick, 1000);
}

function onTimerTick() {
  inactiveTime++;
  document.getElementById("inactiveTime").textContent = inactiveTime;
  if (inactiveTime % 300 == 0) {
    alert("Hey there! Are you still planning to buy something?");
  }
}

function showItem(productName) {
  return function() {
    alert(productName + ": " + cart[productName]);
  };
}

function showCart() {
  var i = 0;
  for (var productName in cart) {
    if (cart[productName] != 0) {
      setTimeout(showItem(productName), i * 30000);
      i++;
    }
  }
  return false;
}

function showOverlay() {
  document.getElementById("overlay").style.visibility = "visible";
}

function updateModalRow(productName) {
  var tr = document.getElementById(productName + "ModalRow");
  var quantity = cart[productName];
  var price = products[productName]["price"] * quantity;
  if(cart[productName] == 0){
    tr.style.display = "none";
  } else {
    tr.style.display = "table-row";
  }
  tr.childNodes[1].innerHTML = quantity;
  tr.childNodes[2].innerHTML = price;
}

function closeModal() {
  document.getElementById("overlay").style.visibility = "hidden";
}

function hideCartModal(e) {
  if (e && e.keyCode == 27) {
    closeModal();
  }
}

function onClickAdd(productName) {
  return function() {
    addToCart(productName);
  };
}

function onClickRemove(productName) {
  return function() {
    removeFromCart(productName)
  }
}

var inactiveTime = 0;
var inactiveTimer = setInterval(onTimerTick, 1000);

document.getElementById("showCart").addEventListener("click", showCart, false);
document.getElementById("cartPriceContainer").addEventListener("click", showOverlay, false);
document.getElementById("closeModal").addEventListener("click", closeModal, false);

for (var productName in products) {
  var idAdd = productName + "Add";
  document.getElementById(idAdd).addEventListener("click", onClickAdd(productName), false);
  var idRemove = productName + "Remove";
  var removeButton = document.getElementById(idRemove);
  removeButton.addEventListener("click", onClickRemove(productName), false);
  removeButton.style.display = "none";
}

var tbl = document.getElementById("modalTable");
for (var productName in cart) {
  var quantity = cart[productName];
  var price = products[productName]["price"] * quantity;
  var tr = tbl.insertRow();
  tr.id = productName + "ModalRow";
  tr.innerHTML = "<td>" + productName + "</td><td>" + quantity + "</td><td>" + price + "</td><td><span class='add'>+</span>/<span class='remove'>-</span></td>";
  tr.childNodes[3].childNodes[2].addEventListener("click", onClickRemove(productName), false);
  tr.childNodes[3].childNodes[0].addEventListener("click", onClickAdd(productName), false);
  tr.style.display = "none";
}

document.addEventListener("keydown", hideCartModal, false);

