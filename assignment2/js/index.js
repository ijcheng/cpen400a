var cart = {};
var products = {
  "Box1": 9,
  "Box2": 9,
  "Clothes1": 9,
  "Clothes2": 9,
  "Jeans": 9,
  "Keyboard": 9,
  "KeyboardCombo": 9,
  "Mice": 9,
  "PC1": 9,
  "PC2": 9,
  "PC3": 9,
  "Tent": 9
};


var inactiveTime = 0;
var inactiveTimer = setInterval(onTimerTick, 1000);

function addToCart(productName) {
  if (products[productName] == 0) {
    alert("The website does not have anymore " + productName + ", fool!");
  } else if (productName in cart) {
    cart[productName]++;
    products[productName]--;
    resetTimer();
  } else {
    cart[productName] = 1;
    products[productName]--;
    resetTimer();
  }
}

function removeFromCart(productName) {
  if (productName in cart && cart[productName] == 1) {
    delete cart[productName];
    products[productName]++;
    resetTimer();
  } else if (productName in cart) {
    cart[productName]--;
    products[productName]++;
    resetTimer();
  } else {
    alert("Product does not exist in the cart, fool!");
  }
}

function resetTimer() {
  clearInterval(inactiveTimer);
  inactiveTime = 0;
  inactiveTimer = setInterval(onTimerTick, 1000);
}

function onTimerTick() {
  inactiveTime++;
  if (inactiveTime % 30 == 0) {
    alert("Hey there! Are you still planning to buy something?");
  }
}

function showItem(productName) {
  return function() {
    alert(productName + ": " + cart[productName]);
  }
}

function showCart() {
  var i = 0;
  for (var productName in cart) {
    setTimeout(showItem(productName), i * 30000);
    i++;
  }
}

