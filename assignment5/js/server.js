
var http = require("http");
if (!http) process.exit(1);

var fs = require("fs");
var MongoClient = require("mongodb").MongoClient;
var getProducts = require("./products").getProducts
var addOrder = require("./orders").addOrder

var serveRequest = function(request, response) {
  console.log("Received " + request.method + " " + request.url);
  if (request.url == "/products") {
    getProducts(response);
  } else if (request.url == "/checkout" && request.method == "POST") {
    var params = '';
    request.on("data", function(data) {
      params += data;
    });
    request.on("end", function() {
      addOrder(JSON.parse(params), response);
    });
  } else if (request.url == "/") {
    response.writeHead(200, {'Content-Type': "text/html"});
    response.end(fs.readFileSync("./index.html"));
  } else if (request.url.substring(0, 3) == "/js" || request.url.substring(0, 7) == "/images" || request.url.substring(0, 4) == "/css") {
    if (request.url.substring(0, 3) == "/js") {
      response.writeHead(200, {'Content-Type': "text/javascript"});
    } else if (request.url.substring(0, 7) == "/images") {
      response.writeHead(200, {'Content-Type': "images/png"});
    } else if (request.url.substring(0, 4) == "/css") {
      response.writeHead(200, {'Content-Type': "text/css"});
    }
    var filePath = "." + request.url;
    if (fs.existsSync(filePath)) {
      response.end(fs.readFileSync(filePath));
    } else {
      response.end();
    }
  } else {
    response.end();
  }
}

var port = 8080;
var server = http.createServer(serveRequest);
server.listen(port);
console.log("Starting server on port " + port);
