
var fs = require("fs");
var MongoClient = require("mongodb").MongoClient;

var content = fs.readFileSync("products.txt");
var json = JSON.parse(content);

MongoClient.connect("mongodb://localhost:27017/products", function(err, db) {
  if (err) throw err;

  var products = db.collection("products");
  products.deleteMany();
  for (var name in json) {
    products.insert({
      "name": name,
      "price": json[name]["price"],
      "quantity": json[name]["quantity"],
      "image": json[name]["url"],
    });
  }
  products.find().each(function(err, product) {
    if (err) throw err;
    if (product) {
      console.log(product);
    } else {
      db.close();
    }
  });
});

