
var MongoClient = require("mongodb").MongoClient;

function addOrder(order, response) {
  MongoClient.connect("mongodb://localhost:27017/products", function(err, db) {
    if (err) throw err;

    var orders = db.collection("orders");
    orders.insert(order);
    db.close();
    response.end();
  });
}

module.exports.addOrder = addOrder;
