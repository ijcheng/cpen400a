
var MongoClient = require("mongodb").MongoClient;

function getProducts(response) {
  MongoClient.connect("mongodb://localhost:27017/products", function(err, db) {
    if (err) throw err;

    var products = {}
    db.collection("products").find().each(function(err, product) {
      if (err) throw err;
      if (product) {
        products[product["name"]] = {
          "price": product["price"],
          "quantity": product["quantity"],
          "url": product["image"],
        };
      } else {
        response.write(JSON.stringify(products));
        db.close();
        response.statusCode = 200;
        response.end();
      }
    });
  });
}

module.exports.getProducts = getProducts;
