
var MongoClient = require("mongodb").MongoClient;

MongoClient.connect("mongodb://localhost:27017/products", function(err, db) {
  if (err) throw err;

  var orders = db.collection("orders");
  orders.find().each(function(err, order) {
    if (err) throw err;
    if (order) {
      console.log(order);
    } else {
      db.close();
    }
  });
});

